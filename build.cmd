@echo off

mkdir bin

echo Building Windows Binary...

go build -o bin\gotex.exe

echo Building Linux Binary...

set GOARCH=amd64
set GOOS=linux

go build -o bin\gotex