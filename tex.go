package main

import (
	"errors"
	"os/exec"
	"strings"
)

func pdftex(tex, outputDir string) ([]byte, error) {
	if !commandExists("pdflatex") {
		return nil, errors.New("pdflatex is not installed")
	}

	args := strings.Split("-output-directory="+outputDir+" -aux-directory="+trashCacheDir + " " + tex, " ")
	cmd := exec.Command("pdflatex", args...)
	
	return cmd.CombinedOutput()
}

func commandExists(cmd string) bool {
	_, err := exec.LookPath(cmd)
	return err == nil
}