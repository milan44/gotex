package main

import (
	"path/filepath"
	"os"
	"time"
)

func startCleanup() {
	ticker := time.NewTicker(1 * time.Minute)
	go func() {
		cleanup()
		
		for {
			<- ticker.C
			
			cleanup()
		}
	}()
}

func cleanup() {
	doFileCleanup(pdfCacheDir, getTooOldFunc(24*time.Hour))
	doFileCleanup(errorCacheDir, getTooOldFunc(24*time.Hour))
	
	doFileCleanup(texCacheDir, getTooOldFunc(15*time.Minute))
	
	doFileCleanup(trashCacheDir, getTooOldFunc(5*time.Minute))
}

func doFileCleanup(dir string, tooOldFunc func(time.Time) bool) {
	filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		
		if info.Mode().IsRegular() {
			if tooOldFunc(info.ModTime()) {
				os.RemoveAll(path)
			}
		}
		
		return nil
	})
}

func getTooOldFunc(d time.Duration) func(time.Time) bool {
	return func(t time.Time) bool {
		return time.Now().Sub(t) > d
	}
}