package main

import (
	"io/ioutil"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
)

func cerror(msg string, c *gin.Context) {
	c.String(http.StatusInternalServerError, msg)
}

func showErrorIfAble(hash string, c *gin.Context) bool {
	errFile := getTrackErrorFile(hash)
	
	if fileExists(errFile) {
		c.Status(http.StatusInternalServerError)
		c.File(errFile)
		return true
	}
	
	return false
}
func getTrackErrorFile(hash string) string {
	return errorCacheDir + string(os.PathSeparator) + hash + ".error"
}
func trackError(hash, msg string, c *gin.Context) {
	ioutil.WriteFile(getTrackErrorFile(hash), []byte(msg), 0777)
	
	cerror(msg, c)
}