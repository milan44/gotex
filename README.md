## Requirements

### Ubuntu, Linux Mint

##### 1. Register GPG key

```
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys D6BC243565B2087BC3F897C9277A7293F59E4889
```

##### 2. Register installation source

Ubuntu 18.04, Linux Mint 19

```
echo "deb http://miktex.org/download/ubuntu bionic universe" | sudo tee /etc/apt/sources.list.d/miktex.list
```

Ubuntu 16.04, Linux Mint 18.3

```
echo "deb http://miktex.org/download/ubuntu xenial universe" | sudo tee /etc/apt/sources.list.d/miktex.list
```

##### 3. Install MiKTeX

```
sudo apt-get update
sudo apt-get install miktex
```

### Debian 9

##### 1. Register GPG key

```
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys D6BC243565B2087BC3F897C9277A7293F59E4889
```

##### 2. Register installation source

```
echo "deb http://miktex.org/download/debian stretch universe" | sudo tee /etc/apt/sources.list.d/miktex.list
```

##### 3. Install MiKTeX

```
sudo apt-get update
sudo apt-get install miktex
```

### [Other Linux based OS](https://miktex.org/howto/install-miktex-unx)

### Windows

Download and run the installer ([click here](https://miktex.org/download)).

Make sure you select "Yes" at the settings step:  
![screenshot](img/select-yes.png)