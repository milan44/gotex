package main

import (
	"net/http"
	
	"github.com/gin-gonic/gin"
)

const (
	cacheDir    = "cache"
	pdfCacheDir = "cache/pdf"
	texCacheDir = "cache/tex"
	errorCacheDir = "cache/error"
	trashCacheDir = "cache/trash"
)

func main() {
	mkdir(cacheDir)
	mkdir(pdfCacheDir)
	mkdir(texCacheDir)
	mkdir(errorCacheDir)
	mkdir(trashCacheDir)
	
	startCleanup()
	
	gin.SetMode(gin.ReleaseMode)

	r := gin.Default()
	
	r.GET("/pdf/url", pdfUrl)
	r.POST("/pdf/text", pdfText)
	
	r.POST("/pdf/url", explain("Use GET for this endpoint"))
	r.GET("/pdf/text", explain("Use POST for this endpoint"))
	
	r.Run(":3000")
}

func explain(text string) func(*gin.Context) {
	return func(c *gin.Context) {
		c.String(http.StatusOK, text)
	}
}