package main

import (
	"io/ioutil"
	"net/http"
	"net/url"
	"os"

	"github.com/gin-gonic/gin"
)

func pdfUrl(c *gin.Context) {
	uri := c.Query("url")
	_, err := url.ParseRequestURI(uri)

	if err != nil {
		cerror("Invalid or no url (?url=[your-url])", c)
		return
	}

	resp, err := http.Get(uri)
	if err != nil {
		cerror("Failed to load file from url", c)
		return
	}

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		cerror("Failed to read body from url", c)
		return
	}

	texStringToPdfHandler(string(b), c)
}

func pdfText(c *gin.Context) {
	text := c.PostForm("text")
	if text == "" {
		cerror("No text (Post parameter text=[your-text])", c)
		return
	}
	
	texStringToPdfHandler(text, c)
}

func texStringToPdfHandler(body string, c *gin.Context) {
	file := getFileHash(body)
	
	pdf := pdfCacheDir + string(os.PathSeparator) + file + ".pdf"
	tex := texCacheDir + string(os.PathSeparator) + file + ".tex"
	
	if showErrorIfAble(file, c) {
		return
	}
	
	if !fileExists(pdf) {
		err := ioutil.WriteFile(tex, []byte(body), 0777)
		if err != nil {
			cerror("Failed to save tex file", c)
			return
		}
		
		out, err := pdftex(tex, pdfCacheDir)
		if err != nil {
			trackError("Error converting to pdf ("+err.Error()+")\n\n" + string(out), file, c)
		
			if fileExists(pdf) {
				os.RemoveAll(pdf)
			}
			return
		}
	
		if !fileExists(pdf) {
			trackError("Something went wrong:\n\n" + string(out), file, c)
		}
	}
	
	c.File(pdf)
}
