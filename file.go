package main

import (
	"crypto/sha1"
	"encoding/base64"
	"os"
)

func fileExists(file string) bool {
	_, err := os.Stat(file)
	return err == nil
}

func mkdir(path string) {
	if !fileExists(path) {
		os.MkdirAll(path, 0777)
	}
}

func getFileHash(data string) string {
	hasher := sha1.New()
    hasher.Write([]byte(data))
    return base64.URLEncoding.EncodeToString(hasher.Sum(nil))
}